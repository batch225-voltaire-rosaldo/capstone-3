import React from 'react';
import './App.css';

import { useState, useEffect } from 'react'
import { UserProvider } from './UserContext'
import AppNavbar from './components/AppNavbar'
import Home from './pages/Home'
import AdminDashboard from './pages/AdminDashboard';
import Products from './pages/Products';
import ProductView from './components/ProductView'

import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import ErrorPage from './pages/ErrorPage'
// import Footer from './components/Footer'
import PreFooter from './components/PreFooter';
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }
  useEffect(() => {

      fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
        headers: {
          Authorization: `Bearer ${ localStorage.getItem('token') }`
        }
      })
      .then(res => res.json())
      .then(data => {
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        } else {
          setUser({
            id: null,
            isAdmin: null
          });
        }
      })
  }, []);

  return (
    <>
      <div className="background-image">
      <UserProvider value={{user, setUser, unsetUser}}>
      {/* <img src="images/background-gradient.jpg" alt="bg-pic"/> */}
        <Router>
        {/* <img src={bg_picture} alt="bg-pic"/> */}
          <AppNavbar/>
          <Container>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/product" element={<Products/>}/>
              <Route path="/product/:productId" element={<ProductView/>}/>
              <Route path="/admin" element={<AdminDashboard/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/logout" element={<Logout/>} />
              <Route path="*" element={<ErrorPage/>}/>
            </Routes>
          </Container>
          {/* <Footer/> */}
          <PreFooter/>
        </Router>
      </UserProvider>
      </div>
    </>
  );
}

export default App;
