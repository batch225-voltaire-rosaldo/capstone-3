import React from 'react'

const UserContext = React.createContext() //Base

export const UserProvider = UserContext.Provider;

export default UserContext
