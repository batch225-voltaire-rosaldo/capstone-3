import React from 'react'
// import Navbar from 'react-bootstrap/Navbar'
// import Nav from 'react-bootstrap/Nav'
import { Row, Container, Box, Heading, Column, FooterLink } from './FooterStyles';

export default function Footer() {
    return (
        <Box>
            <Container>
                <Row>
                    <Column>
                    <Heading>Social Media</Heading>
                    {/* <FooterLink href="#"> */}
                    <i class="fab fa-facebook fa-2x">
                    {/* <span style={{ marginLeft: "10px" }}>
                    Facebook
                    </span> */}
                    </i>
                    {/* </FooterLink> */}
                    <FooterLink href="#">
                    <i className="fab fa-instagram">
                    <span style={{ marginLeft: "10px" }}>
                    Instagram
                    </span>
                    </i>
                    </FooterLink>
                    <FooterLink href="#">
                    <i className="fab fa-twitter">
                    <span style={{ marginLeft: "10px" }}>
                    Twitter
                    </span>
                    </i>
                    </FooterLink>
                    <FooterLink href="#">
                    <i className="fab fa-youtube">
                    <span style={{ marginLeft: "10px" }}>
                    Youtube
                    </span>
                    </i>
                    </FooterLink>
                    </Column>
                </Row>
            </Container>
        </Box>
    );
}
    