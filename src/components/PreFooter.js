import React from 'react'
// import { Button, Row, Col } from 'react-bootstrap';

export default function PreFooter() {
  return (
    <footer>
		<span>Terms of Service</span>
		<span>|</span>
		<span>Privacy Policy</span>
		<span>|</span>
		<span>&copy; 2023 NCP. All Rights Reserved</span>
	</footer>
  )
}

// export default PreFooter
