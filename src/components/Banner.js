import React from 'react';
// import { useState, useEffect, useContext } from 'react'
import { Row, Col } from 'react-bootstrap';
// import UserContext from '../UserContext'
// import { useContext } from 'react'
// import {useState, useEffect, useContext} from 'react'
// import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function Banner() {
    
    // const {user, setUser} = useContext(UserContext);

    // const [email, setEmail] = useState('')
    // const [password, setPassword] = useState('')

    // const [isActive, setIsActive] = useState(false)

    // const retrieveUser = (token) => {
    //     fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    //         headers: {
    //             Authorization: `Bearer ${token}`
    //         }
    //     })
    //     .then(response => response.json())
    //     .then(result => {

    //         setUser({
    //             id: result._id,
    //             isAdmin: result.isAdmin
    //         })
    //     })
    // }

    // function authenticate(event){
    //     event.preventDefault()
        
    //     fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //         body: JSON.stringify({
    //             email: email,
    //             password: password
    //         })
    //     })
    //     .then(response => response.json())
    //     .then(result => {
    //         if(typeof result.access !== "undefined"){
    //             localStorage.setItem('token', result.access)

    //             retrieveUser(result.access)

    //             Swal.fire({
    //                 title: 'Login Successful!',
    //                 icon: 'success',
    //                 text: 'Welcome to GamingLaptop.ph!'
    //             })
    //         } else {
    //             Swal.fire({
    //                 title: 'Authentication Failed!',
    //                 icon: 'error',
    //                 text: 'Invalid Email or password'
    //             })
    //         }
    //     })
    // }

    // useEffect(() => {
    //     if((email !== '' && password !== '')){
    //         setIsActive(true)
    //     } else {
    //         setIsActive(false)
    //     }
    // }, [email, password])
    
    return (
        <Row className="mt-4 mb-4">
            <Col>
                <h2>Shop your dream Laptop</h2>
                {/* <p>Create, Play, Buy Now.</p> */}

 
                <Link className="btn btn-danger btn-block" to="/product">Buy Now!</Link>
                {/* <Button variant="danger">Shop Now!</Button> */}
            </Col>
        </Row>
    )
}