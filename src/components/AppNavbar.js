import React from 'react';
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { Link, NavLink } from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'

export default function AppNavbar(){

	const {user} = useContext(UserContext)

	return(
		<Navbar className="p-4" bg="light" expand="lg" sticky="top">
			<Navbar.Brand className="ml-3" as={Link} to="/">GamingLaptop.ph</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={NavLink} to="/">Home</Nav.Link>
					{/* <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link> */}
					<Nav.Link as={NavLink} to="/product">Products</Nav.Link>
					{/* {	(user.isAdmin == true) ?
						
						<Nav.Link></Nav.Link>
						:
						<Nav.Link as={NavLink} to="/admin">Admin</Nav.Link>
					} */}
					{	(user.id) ?
						<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
						:
						<>
							<Nav.Link className="d-flex justify-content-end" as={NavLink} to="/login">Login</Nav.Link>
							<Nav.Link className="d-flex justify-content-end" as={NavLink} to="/register">Register</Nav.Link>
						</>
					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}