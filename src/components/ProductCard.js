import React from 'react';
import {Card, CardImg} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function ProductCard(product){

  const {name, description, price, image, _id} = product.productProp

  return(
    <>
    <h2>{name}</h2>
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PHP {price}</Card.Text>
        <Card.Subtitle>Image</Card.Subtitle>
        <CardImg top src={image}/>
        <Link className="btn btn-primary" to={`/product/${_id}`}>Details</Link>
      </Card.Body>
    </Card>
    </>
  )
}
