import React from 'react';
import '../App.css';
import { Row, Col, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function Highlights () {

    return (
        <Link to="/product" style={{textDecoration: 'none'}}>
        <Row className="mt-4 mb-4">
        <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h3>Apple Products</h3>
                    </Card.Title>
                    <img className="container-fluid bg-none" id="picture1" />
                    {/* <Card.Img id="picture1" className="container-fluid bg-none"  /> */}
                    <Card.Text>
                        See more Macbook.
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
        <Col xs={12} md={4} >
        {/* <img className="container-fluid bg-none" id="picture2" /> */}
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h3>Windows Laptop</h3>
                    </Card.Title>
                    <img className="container-fluid bg-none" id="picture2" />
                    <Card.Text>
                        See more Windows Laptop.
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
        <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h3>Special Offers</h3>
                    </Card.Title>
                    <img className="container-fluid bg-none" id="picture3" />
                    <Card.Text>
                        Click for Special Offers.
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
        </Row>
        </Link>
    )
}