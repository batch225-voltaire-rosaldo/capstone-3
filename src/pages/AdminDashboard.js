import React from 'react';
import AdminBanner from '../components/AdminBanner';
// import Highlights from '../components/Highlights';
import ProductCard from '../components/ProductCard';

import CourseCard from '../components/CourseCard'
import Loading from '../components/Loading'
import {useEffect, useState} from 'react'

export default function AdminDashboard() {
    const [products, setProducts] = useState([])
	const [isLoading, setIsLoading] = useState(true)

	useEffect((isLoading) => {

		fetch(`${process.env.REACT_APP_API_URL}/product/`)
		.then(response => response.json())
		.then(result => {
			console.log(result)
			setProducts(
				result.map(product => {
					return (
						<ProductCard key={product._id} productProp ={product}/>
					)
				})
			)
			setIsLoading(false)
		})
	}, [])

	return(			
			(isLoading) ?
				<Loading/>
			:
			<>
                <AdminBanner />
				{products}
			</>
	)

    // return (
    //     <>
    //         <AdminBanner />
    //         {/* <Highlights /> */}

    //     </>
    //     )
}
