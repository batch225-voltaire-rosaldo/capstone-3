import React from 'react';
import ProductCard from '../components/ProductCard';

import CourseCard from '../components/CourseCard'
import Loading from '../components/Loading'
import {useEffect, useState} from 'react'
import UserContext from '../UserContext'

export default function Products(){
	const [products, setProducts] = useState([])
	const [isLoading, setIsLoading] = useState(true)

	const [isActive, setIsActive] = useState([])

	useEffect((isLoading) => {

		fetch(`${process.env.REACT_APP_API_URL}/product/`)
		.then(response => response.json())
		.then(result => {
			console.log(result)
			setProducts(
				result.map(product => {
					return (
						(isActive) ?
						<ProductCard key={product._id} productProp ={product}/>
						:
						<></>
					)
				})
			)
			setIsLoading(false)
		})
	}, [])

	return(			
			
			(isLoading) ?
				<Loading/>
			:
			<>
				<h1 className="product-name">Our Products</h1>
				{products}
			</>
	)
}
